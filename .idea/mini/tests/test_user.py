import unittest
from sqlalchemy import create_engine
from sqlalchemy import text


class TestSQLQueries(unittest.TestCase):
    def test_user(self):
        db_string = 'postgresql://habrpguser:pgpwd4habr@postgres:5432/habrdb'
        db = create_engine(db_string)
        with db.engine.connect() as conn:
            print('User есть')
            query = conn.execute(text("" + \
                                      "SELECT fullname, username, email, password " + \
                                      "FROM users " + \
                                      "WHERE fullname is not null " + \
                                      "LIMIT 1"))

        result_set = query
        for (r) in result_set:
            return r[0]

    def test_account(self):
        db_string = 'postgresql://habrpguser:pgpwd4habr@postgres:5432/habrdb'
        db = create_engine(db_string)
        with db.engine.connect() as conn:
            print('Account тут')
            query = conn.execute(text("" + \
                                      "SELECT account_number, username " + \
                                      "FROM accounts " + \
                                      "WHERE account_number is not null " + \
                                      "LIMIT 1"))

        result_set = query
        for (r) in result_set:
            return r[0]


if __name__ == '__main__':
    unittest.main()
