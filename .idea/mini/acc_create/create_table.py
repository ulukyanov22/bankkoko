import psycopg2

conn = psycopg2.connect(dbname="habrdb", user="habrpguser", password="pgpwd4habr", host="172.17.0.1")
cursor = conn.cursor()

conn.autocommit = True
# команда для создания базы данных metanit
sql = "CREATE DATABASE TEST"

# выполняем код sql
cursor.execute(sql)
print("База данных успешно создана")

cursor.close()
conn.close()