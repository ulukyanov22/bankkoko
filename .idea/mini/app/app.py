import time
import random

from sqlalchemy import create_engine
from sqlalchemy import text

# Connect to the database
db_string = 'postgresql+psycopg2://habrpguser:pgpwd4habr@postgres:5432/habrdb'
db = create_engine(db_string)


def add_new_row(n):
    # Insert a new number into the 'numbers' table.
    with db.engine.begin() as conn:
        conn.execute(text("INSERT INTO numbers (number,timestamp) " + \
                          "VALUES (" + \
                          str(n) + "," + \
                          str(int(round(time.time() * 1000))) + ");"))


def get_last_row():
    # Retrieve the last number inserted inside the 'numbers'
    with db.engine.connect() as conn:
        query = conn.execute(text("" + \
            "SELECT number " + \
            "FROM numbers " + \
            "WHERE timestamp >= (SELECT max(timestamp) FROM numbers)" + \
            "LIMIT 1"))

    result_set = query
    for (r) in result_set:
        return r[0]


if __name__ == '__main__':
    print('Application started')

    while True:
        add_new_row(random.randint(1, 100000))
        print('The last value insterted is: {}'.format(get_last_row()))
        time.sleep(5)
