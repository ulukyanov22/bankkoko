import psycopg2

conn = psycopg2.connect(dbname="habrdb", user="habrpguser", password="pgpwd4habr", host="postgres")
cursor = conn.cursor()

conn.autocommit = True
# команда для создания базы данных metanit
sql1 = "CREATE DATABASE HABRDB;"

# выполняем код sql
cursor.execute(sql1)
print("База данных успешно создана")

cursor.close()
conn.close()

conn = psycopg2.connect(dbname="habrdb", user="habrpguser", password="pgpwd4habr", host="postgres")
cursor = conn.cursor()

# команда для создания базы данных metanit
sql2 = ("CREATE TABLE public.users (fullname VARCHAR (50) UNIQUE NOT NULL, username VARCHAR (50) UNIQUE NOT NULL, "
        "email VARCHAR (50), password VARCHAR (50) UNIQUE NOT NULL);"
        "INSERT INTO users (fullname, username, email, password) VALUES ('Анs', 'ue', 'ан', 'пар');")

# выполняем код sql
cursor.execute(sql2)
conn.commit()
print("Таблица users успешно создана данные залиты")

sql3 = ("CREATE TABLE public.accounts (account_number VARCHAR (50) UNIQUE NOT NULL, username VARCHAR (50) UNIQUE NOT NULL);"
        "INSERT INTO accounts (account_number, username) VALUES ('4444555566667777', 'Ан1');")

# выполняем код sql3
cursor.execute(sql3)
conn.commit()
print("Таблица accounts успешно создана данные залиты")

cursor.close()
conn.close()
