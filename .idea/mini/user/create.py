import psycopg2

conn = psycopg2.connect(dbname="postgres", user="habrpguser", password="pgpwd4habr", host="habrdb")
cursor = conn.cursor()

conn.autocommit = True
# команда для создания базы данных TEST
sql = "CREATE DATABASE TEST"
sql = "CREATE TABLE users (fullname VARCHAR (50) UNIQUE NOT NULL, username VARCHAR (50) UNIQUE NOT NULL, email VARCHAR (50), password VARCHAR (50) UNIQUE NOT NULL) FROM test;"

# выполняем код sql
cursor.execute(sql)
print("База данных успешно создана")

cursor.close()
conn.close()